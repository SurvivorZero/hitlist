package us.theappacademy.hitlist;

public class ToDoItem {
    public String title;
    public String assigned;
    public String due;
    public String category;

    public ToDoItem(String title, String assigned, String due, String category) {
        this.title = title;
        this.assigned = assigned;
        this.due = due;
        this.category = category;
    }
}