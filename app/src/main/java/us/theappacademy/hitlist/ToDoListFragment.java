package us.theappacademy.hitlist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem i1 = new ToDoItem("Diego", "6/9", "9/6", "Special Delivery");
        ToDoItem i2 = new ToDoItem("Blake", "9/11", "9/11", "Allahu Akbar");
        ToDoItem i3 = new ToDoItem("Luis", "4/20", "idk fam", "Camping");
        ToDoItem i4 = new ToDoItem("Henry", "6/6", "6/6", "Ritual");

        activity.todoItems.add(i1);
        activity.todoItems.add(i2);
        activity.todoItems.add(i3);
        activity.todoItems.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activityCallback);
        recyclerView.setAdapter(adapter);

        return view;
    }
}