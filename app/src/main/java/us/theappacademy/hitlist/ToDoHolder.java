package us.theappacademy.hitlist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ToDoHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public ToDoHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
